# Jupyter

```{warning}
Page on active development
```
## Introduction
TBminer-ng objectives are take prédictions in different MTBC lineage models.
It is a classification problem with categorical data.

## Datasets
From a precedent work, we have a dataset of label data

 -> datasets descriptions

## This subproject goal is: 
 * define a pipeline for training/validating/testing and use the best models
 * Research the best machine learning models
 * Optimise select model by finding the best hyperparameters 

## Project structure

````bash
.
├── analyseResults       # Generate basic analyse of result ( CV search and TEST)
│   └── AnalyseResult_all_all.ipynb
├── data                 # Data use for pipeline
├── generate_parameters  # Generate best parameter (for import on webapp)
│   ├── Generate_best_param_all.ipynb # generate best hyperparameters
│   └── clf_best_param.yaml           # file to export on webapp
├── model                # Store model
├── model_config         # General configuration
├── models               # Store model
├── results              # Output of CV search (prediction/learning curve/...)
└── runCSSearch          # Run CV search 
    └── xgboost_cvSearch_all.ipynb
````

The standard process was defined the hyperparameter search map on "xgboost_cvSearch_all.ipynb" notebook :
````python
max_depths = [5,10,20]
early_stopping_rounds_list = [20,50,70,90]
subsample_list = [0.2,0.4,0.6,0.8, 1]
````
And run all the notebooks.

When finished, run the "Generate_best_param_all.ipynb" notebook to get the best hyperparameters for all couples (input, output)

Then, "AnalyseResult_all_all.ipynb" uses the best hyperparameters to train the model with *train data*, and test the model with *test data*.

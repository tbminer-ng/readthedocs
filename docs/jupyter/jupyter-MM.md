## Materials and methods
### Isolate
We reuse isolates from the first TBminer study in this study (Aze et al., 2015). The dataset has:
Three thousand four hundred and fifty-four isolates (n = 3,454) were from the National Reference Center of Netherlands, also committed to worldwide quality control studies.

### Data presentation
#### Explanatory variables
In this study, we accepted the two types of data, spoligotyping and MIRU-VNTR, to generate prediction.
VNTR
Originally based on 12 loci, this method proposed two sets of the locus for better performance: VNTR15 with fifteen loci and VNTR24 with twenty-four loci. All the VNTR15 loci are included in the VNTR24 locus set. TBminer-ng accepts both VNTR15 and VNTR24. Each VNTR is an integers characters.
Spoligotypes
We use the 43 spacers models, consisting of a 43 booleen characters link to the presence/absence of the  spacers.

#### Response variable
##### Lineage
Lineage classification consists of 7 clades.
##### MIRU-VNTRPlus 
MIRU-VNTRPlus classification consists of 18 clades
##### SITVITWEB
SITVITWEB classification consists of 52 clades
##### Consensus Expert
Consensus expert classifications consists of 24 clades.

### Metrics
#### Accuracy
When evaluating the performance of machine learning models, accuracy is one of the most used metrics. It measures the percentage of correctly classified instances compared to the total number of instances. However, more than accuracy is required to evaluate the effectiveness of a model, as it can be affected by class imbalance or skewed data. Accuracy has been calculated for information : some other studies use it, like the first version of TBminer and to test relation between predict probability and accuracy.
On multiclass classification, like TBminer-ng prediction, acuracy is define as :
Accuracy=(correct classifications)/(all classifications)

#### F1-score
The f1 score is a measure of a model’s performance that takes into account both precision and recall. It is a valuable metric for evaluating classification models, especially when the classes are imbalanced. A high f1 score indicates a model has high precision and recall, meaning it correctly identifies and classifies the most relevant instances. On the other hand, a low f1 score suggests that a model either has poor precision (i.e., it identifies too many false positives) or poor recall (i.e., it misses too many true positives). Overall, the f1 score is a valuable tool for assessing the performance of machine learning models and improving their accuracy. The F1 score was calculated for each predicted class, and the overall score was the mean of all scores. Thus, all predicted class have the same weight. These metrics were used as train metrics for machine learning models and compare models which each other.
F1 score=2*(precision*recall)/(precision+recall)
  
With :  
Precision=(True positive)/(True positive+False positive)
And  
Recall=(True positive)/(True positive+False negative)

#### classification report
Classifications reports gave detailed metrics for classification results. (accuracy, precision, recall, f1-score for all target classes and samples with different methods). It is helpful for fine analysis but complex for comparing models to each other due to a variable number of metrics computed, depending on the sample used for classifications.

#### Confusion matrix
The confusion matrix, in combination with a heat plot, was an excellent graphical representation of the model’s performance. The diagonal elements represent the number of points for which the predicted label equals the valid label, while the classifier mislabels off-diagonal elements. The higher the confusion matrix’s diagonal values, the better, indicating many correct predictions. Due to imbalanced data, normalisation by class support size (number of elements in each class) is computed.
Prediction probabilities
The train model gave two interesting outputs when predicting a sample: target classes assignation and probability of this assignation. We verify high probability (> 90%) given by the model corresponds to a high f1-score(>90 %). Moreover, we test a linear regression for prediction ~ (prediction probability).
Machine learning model: XGBoost  
Xgboost is a popular algorithm for machine learning tasks. It’s known for its efficient implementation of gradient boosting, a powerful technique for improving the accuracy of predictive models. Xgboost can handle large datasets and perform well on various problems, including classification and regression.
XGboost is an Ensemble Classifier, the final prediction depend on several simple estimator. It’s an Boosting Classifier, so each estimator isn’t independent of other. For each iteration of the model, a new estimator is train with objective to minimize error from previous iteration.

### Input data
Model could accept four types of data: VNTR15, VNTR24, Spoligotypes (43 spacer) and the association of VNTR24 and spoligotypes.
	VNTR15 was compose of: 
VNTR_0580, VNTR_2996, VNTR_0802, VNTR_0960, VNTR_1644, VNTR_3192, VNTR_0424, VNTR_0577, VNTR_2165, VNTR_2401, VNTR_3690, VNTR_4156, VNTR_2163b, VNTR_1955, VNTR_4052
	VNTR24 was compose of:  
VNTR_0580, VNTR_2996, VNTR_0802, VNTR_0960, VNTR_1644, VNTR_3192, VNTR_0424, VNTR_0577, VNTR_2165, VNTR_2401, VNTR_3690, VNTR_4156, VNTR_2163b, VNTR_1955, VNTR_4052, VNTR_0154, VNTR_2531, VNTR_4348, VNTR_2059, VNTR_2687, VNTR_3007, VNTR_2347, VNTR_2461, VNTR_3171
	Spoligotype was compose of: 
spoligo1, spoligo2, spoligo3, spoligo4, spoligo5, spoligo6, spoligo7, spoligo8, spoligo9, spoligo10, spoligo11, spoligo12, spoligo13, spoligo14, spoligo15, spoligo16, spoligo17, spoligo18, spoligo19, spoligo20, spoligo21, spoligo22, spoligo23, spoligo24, spoligo25, spoligo26, spoligo27, spoligo28, spoligo29, spoligo30, spoligo31, spoligo32, spoligo33, spoligo34, spoligo35, spoligo36, spoligo37, spoligo38, spoligo39, spoligo40, spoligo41, spoligo42, spoligo43

### Predict classification
#### Lineage
Euro-American, Indo-Oceanic, East-African_Indian_(CAS), East_Asian_(Beijing), West_African_1, Mycobacterium_bovis, West_African_2, Mycobacterium_africanum, missing.
#### MIRUVNTRplus
Haarlem, LAM, EAI, Delhi/CAS, Cameroon, Beijing, UgandaI, H37Rv, S, NEW-1, X, West_African_1, TUR, Bovis, URAL, West_African_2, UgandaII, Ghana, missing.
#### SITVITWEB
T1, H3, LAM9, CAS1_DELHI, Beijing, H1, T2, LAM1, EAI2_MANILLA, EAI1_SOM, AFRI_2, EAI6_BGD1, LAM3, EAI5, CAS1_KILI, CAM, EAI3_IND, H2, BOVIS, AFRI_1, URAL2, TUR, S, T3, LAM5, EAI2_NTB, LAM3_S, X1, LAM11_ZWE, URAL1, T5, CAS2, T5_RUS1, LAM4, BOVIS1_BCG, H37RV, LAM8, LAM12_MAD1, T2_UGANDA, T1_RUS2, X3, LAM6EAST_MED1, T3_ETH, T4_CEU1, T5_MAD2, EAI4_VNM, X2, LAM2, , missing.
#### Expert
L0_Animal, L1_EAI, L1_EAI1, L1_EAI2, L1_EAI3, L1_EAI6, L2_Beijing, L3_CAS, L4, L4_CAM, L4_H1-2, L4_H3, L4_H37Rv, L4_LAM, L4_S, L4_T4, L4_T5, L4_Tur, L4_Uganda(T2), L4_Ural1, L4_Ural2(New1), L4_X, L5_Afri2, L6_Afri1, missing,


![img_1.png](img_1.png)  
*Figure 1: Methodology for searching best models*

### Dataset preparation
The original dataset was cleaned: A sample without a label or an inconsistent label (like ‘Unknown’) was removed. Then barcode from the explicative variable was computed. We concatenate all variable values for this, inserting an “x” between each. A sample with a barcode assigned to different labels was removed. Finally, a class with a low number of samples was either removed or duplicated. Indeed, the pipeline used to refine and test models need enough samples by category. The choice of remove or resample classes is based on the expert eyes of G. Refrégier: some labels were ambiguous and removed, and other was references (like H37rv) and oversampling. Sample without valid value was add to on the model (assigned to “missing” class for all classification to ensure empty or wrong input data was don’t assign to class.
Hyperparameter search pipeline
The training pipeline has two significant steps: finding the set of best models hyperparameters., and trained the model with the best set of hyperparameters of the precedent step with the train set and evaluating it.
• Split train/test dataset
 
![img_3.png](img_3.png)
*Figure 2 : 5-folds cross-validations hyperparameters search and testing*

The main goal of ML algorithms is to predict unseen data. To evaluate this performance, the standard method was to split the dataset between the train and test sets. The train set was used to find the best collection of hyperparameters and then to train the model. With the model trained with the training dataset, the test dataset was used to evaluate the performance of the models. 
- Split parameters The split method makes a training dataset of 80% of the dataset and a testing dataset of 20 %. Stratification was used, Indeed all classes had the same proportions on the base, training, and testing datasets. We forced the seed of the random split to permit reproducibility.
- Encoding The original dataset was a mix of different types of data : Integer for VNTR, Boolean for spoligotype and string for labels.
- Machine learning classifiers generally gave better results when data input was Boolean data and label output was a consecutive list of integers. For this, we encoded data input with OneHotEncoder and label data with Labels encoders. OneHotEncoder converted each column of “n” categories into “n” columns of Boolean (presence/absence of these categories) LabelEncoders assigned a number at each label category and transform label columns. 
- To avoid overfitting, we realised cross-validation with the training dataset (Figure 2). Datasets were split (n= 10 folds) with a stratified method and iterate sequence training/validation with one split as validation and 9 split as training. The mean of the F1-score was used to evaluate the best parameters. 
- XGBoost learning process XGBoost had two types of training methods: the first was ‘classic’ and trains the models with the train data until the convergence of the model. The second was to train the model with train data and evaluated the performance of each step with another dataset, the validation dataset. The learning phase stopped when news iterations doesn’t improve metrics based on the validation dataset.On this study used the second method with the validation dataset for the cross-validation process. 
- To optimize learning tasks for improved model performance with f1-score as evaluation metrics, with use a custom learning metrics : (1 -  f1-score). Indeed, model need metrics to minimized and we wanted to maximize f1-score.
- Hyperparameters Some randoms tests noted three hyperparameters significantly impact the models:
  - Early stopping rounds (this parameter indicates the number of iterations the model takes after finding a minimum to avoid stopping models on a local minimum),
  - Subsample: Each iteration uses a subset of the training data (the half if subsample = 0.5). It uses to make training robust to noise.
  - max_depth: Maximum depth of a tree. Increasing this value will make the model more complex and likely to overfit.
- Search hyperparameter processing. For each hyperparameter and each cross-validation step, models were trained with 9/10° of the training dataset and validated with the 1/10° remaining. Then we used the model to predict the label of the validation 1/10°. The f1-score was computed for each split with the predicted and valid labels. Finally, for each set of hyperparameters, we take the mean of the ten f1-score computed by cross-validation. For the best score, the hyperparameter set and the number of estimators were selected for the subsequent phases. 
- The number of estimators select correspond to the number of estimator for the last best iteration (for the validations score). Use this formula ensure we don’t use a too low number of estimators which decrease the correlation between predict probability and accuracy.
-  Final number of estimator was the mean of the 10 value.
- The model was trained again with all the train datasets and three hyperparameters: “subsample,” “max depth,” and “n estimators.” Indeed, we don’t use a validation dataset for this training to stop the model, but the number of estimators previously found. Indeed for final train we don’t used validation dataset to avoid overfitting to it.
- Generate prediction The training model computed prediction classes and associated probability from test data. First, we computed a classification report and get the f1-score, accuracy, number of clades, and test size. Additional data was computed to verify estimate under and overfitting ( learning curve, existing relation between f1-score and sample classes proportions,…)

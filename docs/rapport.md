# Rapport de stage

### <p style="text-align: center;">**Résumé**</p>


## Introduction

La tuberculose, maladie causée par les bacilles du complexe *Mycobacterium tuberculosis* (MTBC),
est la bactérie infectieuse la plus mortelle au monde(WHO, report 2022).
Le génotypage de *M. tuberculosis* est fondamentale pour détecter les clusters et adapter
les traitements afin de réduire la mortalité. À partir de genotypage, il est possible d'associer à chaques isolats
un clade dans une classification existante.  

Des études précédentes ont proposé simultanément une classification et un outil associé dans le but de faire des prédictions
sur cette classification.

Une premiere version de **TBminer** a proposé un outils permettant de réaliser des prédictions sur différente
classification à l'aide de deux groupes de marqueurs génotypiques, VNTR et spoligotype, sur trois classifications de
références ainsi qu'une nouvelle classification recherchant un consensus entre les précédentes classifications. 
**TBminer** proposait des prédictions à partir d'algorithme de classification supervisé entrainé sur un jeu de donnée de
près de 3000 souches.  

La mise à jour du serveur hébergeant l'application la rendu celui-ci incompatible avec celui-ci. La complexité 
de l'application ainsi que l'évolution des algorithmes de machine learning à conduit à la création d'une nouvelle 
application.

L'objectif de cette nouvelle étude est de réaliser un projet comportant l'application de prédiction, 
les scripts permettant de rechercher les meilleurs hyperparametres d'un modeles, la documentation, ainsi que l'outillage 
permettant le developpement et la maintenance de l'application.

La recherche des meilleurs modèles est décrite dans un rapport distinct, nous nous consacrons ici au developpement
 de l'application et des outils.

## Cahier des charges

1. L'application doit être accessible depuis internet via un simple navigateur
2. Les modèles de prédictions doivent être aussi performant que possible
3. La maintenance de l'application doit etre le plus limité possible
4. Le choix du serveur où sera deployé n'est pas connu lors du developement
5. Le projet doit pouvoir etre facilement réutiliser pour mettre à jour les modèles ou l'application.
6. Au minimun , l'application doit accepter des fichiers csv avec les echantillons et retourner un fichier csv avec les prédiction associées

Des contraintes liées aux materiels et logiciels disponibles ont également du etre pris en comptes:
Ordinateur de developement non dédié :
- Ordinateur professionnel avec des droits limités
- Ordinateur personnel de faible puissance
- Hyperviseur accessible depuis internet via un reverse-proxy

Plateforme avec accès libre :
- Gitlab
- Readthedocs.io

## Solutions techniques proposées

### Outils généraux
- Gitlab (auto-hebergé et gitlab.com) :
  - Hebergement des projets
  - Versionning
  - Edition en ligne
  - CI/CD
- JupyterLab (auto-hebergé)
  - Developement des scripts jupyter
  - Recherches des meilleurs hyperparametres
  - Disponiblilité en ligne
- HAproxy (auto-hebergé):
  - Permet l'accessibilité des différents services depuis internet
- Docker :
  - Création d'image de l'application facilement deployable
- Agent gitlab (Machine virtuelle Ubuntu 22.04) :
  - permet le deploiement de l'application sur des machines internes
- Deux machines virtuelles (ubuntu 22.04) :
  - Disponibilité de l'application avec une version stable et une en developpement

### Principaux langage
- Frontend :
  - Typescript
  - CSS
  - HTML
- Backend & Machine Learning :
  - Python
  - yaml
- Documentation :
  - MarkDown
- Deploiement :
  - gitlab-ci
  - docker
  - docker-compose


### Webapplication
La webapplication est publiquement accessible dans un container docker. Celle-ci est entrainé dans le processus de 
deploiement, la mise en place est donc très rapide.  
Ce container ne contient que le minimum necessaire au fonctionnement de l'application.  

#### Technologie
- Frontend : Angular
- Backend : FastAPI, SKlearn, XGboost, pandas


### Recherche du meilleurs modèles 
La recherche des meilleurs models et des hyperparametres associées est réalisée en dehors de l'application, 
par l'utilisation de notebook *jupyter*. Ceux-ci sont disponibles dans le projet. Le processus d'entrainements 
construits un fichier *yaml* definissant les meilleurs hyperparametres,  qui doit manuellement êtres intégrés dans la
webapplications. Ce choix permet 
de pouvoir developper les scripts sur n'importe quelle machines et l'utilisation de la puissance d'une autre machine
pour le calcul.

### Documentation
La documentation est hebergé sur le site readthedoc.io à partir de source en format markdown et notebook jupyter

### Code source
L'ensemble du code source est disponible sur gitlab.com en licence GNU v3.


## WebApplication
L'application est construite a l'aide du framework *angular* pour le front-end, et le framework *fastapi* pour le backend.
N'ayant pas vocation à etre modifié de facon interactive, le choix a été fait de ne pas mettre en place de baser de données.
Cela réduit la comlpexité de maintenance et réduit la surface d'attaque potentiel de l'application.

### Front-end
Le framework *angular* a été choisie pour sa large utilisation et un outillage important et bien documenté. Un projet 
angular necessite d'être compilé avant d'être mis en production. 
Le front-end a pour responsabilité de fournir :
- une interface intuitive à l'utilisateur
- des informations contextuelles sur MTBC, les types de données acceptés, les classifications, le projet...
- un processus de soumission de données avec les vérifications associées
- une présentation des données récupérées

Deux *composants* angular principaux sont présents dans le front-end, construits avec des buts différents :
- le composant *nav* qui présente les informations contextuelles, celle-ci étant les plus probablements modifiés. 
Pour simplifier une éventuelle modification dans cette section, les éléments de celle-ci sont dynamiquement téléchargé 
sur un point de terminaison de l'API REST du backend. Cela permet de limiter les manipulations du code du frontend. 
En effet, à cause de la necessité de compilation de celui-ci, toute mise à jour du code conduit à une compilation 
relativement longue. Ce composant utilise deux modèles : un pour le type de données acceptées depuis l'API REST, 
un autre pour les règles d'affichages de celle-ci.
- le composant *mainContent* accepte le fichier fournit par l'utilisateur, vérifie qu'il sera correctement accepté, 
le soumet au backend pour prédiction et récupère le résultat pour le présenter à l'utilisateur par un téléchargement direct 
ou un affichage paginé en ligne. Ce composant n'a pas vocation à être modifié en dehors d'un contexte de dévellopement.
IL s'agit d'une sequence d'étape, certaine bloquante, permettant de limiter le risque de soumission de données non 
acceptées. La source de données étant un fichier "csv", transformé ultérieurement en dataframe, deux éléments sont 
particulièrement importants : le séparateur et les titres de colonnes. Ces deux éléments sont vérifiés par l'analyse
de la premiere ligne du fichier : celle-ci est transformée en liste en utilisant le séparateur, chaque élément est 
modifié par une expression reguliere. UNe vérification est ensuite faite pour vérifier que la liste contient un des 
ensemble de type de données acceptées. Enfin, pour chaque set de colonnes reconnus, il est proposé à l'utilisateur de 
soumettre le fichier à un point de terminaison de l'API REST du backend avec comme parametre le séparateur.

Les principales variables utilisé sont stocké dans des fichiers regroupé dans un dossier *service*

Le frontend est compilé lors de la construction de l'image docker.

### Back-end
Le framework *FastAPI* a été choisie pour sa simplicité d'emploi. Il permet de créer une API REST en ajoutant un 
approprié à une fonction.
Le projet est composées de quatres partie :
- *main.py* contient le code de l'application
- *test.py* contient les tests associés à l'application
- *train_model.py* contient le code necessaire à entrainer les modèles lors de la phase de construction de l'image docker
- *data/* les données utilisées pour les algorithmes de classifications supervisés et les exemples (fichier csv)
- *frontend/* les données affichées dans la zone latérale du frontend, (fichier yaml)
- *model_config/* les paramètres utilisés dans le modèles

Deux types de methodes sont utilisées dans l'application FastAPI :
- GET : Utilisé pour récuperer une information du backend (redirection, fichier du frontend,
json utiliser par le frontend, exemple de csv.. )
- POST : soumet un fichier au backend et renvoi la prédiction. 


### Construction de l'image
L'image docker est construite en trois étapes :
- L'entrainement du modèle de classifications supervisées et sauvegardes des modèles
- Compilations de l'application angular et sauvegarde des fichiers générer
- Construction de l'image finale à partir des sources (pour le backend) et des deux étapes précédentes
- Réalisation des tests associer au backend afin de vérifier que chaque point de terminaison API fonctionne.

````mermaid
flowchart TD
A[(Dataset)]
B[(Hyperparametre)]
C[Entrainement du modèle]
D[(Modèle entrainé)]

G[code source angular]
H[Code compilé angular]

M[webapplication]
N[webapplication prete pour le deploiement]

subgraph Python image
A --> C
B --> C
C --> D
end

subgraph Node image
G --> |Compilation| H
end

subgraph Python image
H --> M
D --> M
M --> |test des points de terminaisons| N
end
````




### Process de CI/CD

Afin de simplifier la mise en production, tout en augmentant la confiance dans le produit mis en production, une solution
desormais courante est le processus de CI/CD : (continious integration, continious delivery, continious deployment).  
Le principe est d'automatiser l'intégration des modifications jusqu'à la mise en production par un processus controllé.
Les cycles rapides de developpement/mise en condition de production permettent d'éviter l'apparition 
d'incompatibilité a cause de la difference d'environnemnt entre le devellopement et la production.
Pour notre projet, l'environnement de production n'étant pas connu, l'utilisation des conteneurs permet de limiter ce problème :
ceux-ci sont en effet bien définie (type image, version, dépendances installées), et les relations avec l'extérieur du conteneur 
utilise des protocoles réseaux standard (protocole TCP/IP, http, API REST).



````mermaid
flowchart LR
A[code en cours de developpement] === Local
B[code source de l'application]   === GitLab
C[compilation]                    === Runner
D[test]                           === Runner
E[Application mis a jour]
F[Application disponible]         === DockerHub
G[Application en production]      === H[Serveur preprod/production]

subgraph Integration
A --> |commit| B
B --> C
C --> D
D --> E
end
subgraph Continious Delivery
E --> |poussé sur un depot de production|F
end
subgraph Continious Deployment
F --> |mise à jour de l'application|G
end
````











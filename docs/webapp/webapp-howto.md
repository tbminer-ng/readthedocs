# How to 

To use the web application, go to [https://darthos.freeboxos.fr/tbminer-ng](https://darthos.freeboxos.fr/tbminer-ng)

![1_page.png](1_page.png)

The left panel correspond to the general description, and the right panel to the application.

**DownloadSampleData** allows you to download a file with the correct column names and a few samples for testing the application.  
**Next** move on to the next phase

![2_page.png](2_page.png)

**Choose file** button was to select a local CSV file for prediction.    
**Next** button was allowed if the file was submitted  

![3_page.png](3_page.png)

You can select custom *separator* and see if columns were recognized by the application.
By expanding the line, you can see which columns were recognized or not.  
**Next** button was allowed if at least one data input type was recognized.

![4_page.png](4_page.png)

For each input type, you can either directly download the result or view the result on the browser and download it afterwards. On large datasets, online previews need a large amount of resources.

![5_page.png](5_page.png)

If you choose previews, you can download results as CSV or JSON with the button on top.
On the right, the prediction and the associate prediction were shown.

```{warning}
If the prediction probability value is low, check the column's name and value and delete non-essential columns from the submitted dataset.
```

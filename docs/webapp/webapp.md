# Presentation

The **TBminer-ng WebApp** subproject is an online tool used to predict several MTBC (*Mycobacterium tuberculosis* Complex) lineage.  
For this, it can use two types of molecular genotyping information: MIRU-VNTR and Spolygotype.  
The frontend was realized with Angular Framework.  
The backend with Python framework Fastapi.  
The machine learning algorithm is Xgboost.   
The application trains models on the build based on the following: 
 * reference dataset
 * hyperparameter research in the Jupyter subproject.  

It is a docker application.  
Main branch project deployment had a CI/CD: build and push to the staging server was automatic if the test passed.

#### Goals
This documentation goal was explained: 
 * How to locally install the application
 * How to use the application with a web browser
 * The data submission steps for predicting lineage
 * The application architecture
 * The keys class/function/configuration file
 * Deploy a development environment

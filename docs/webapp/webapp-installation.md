# Local installation guide

TBminer-ng is available here : [https://darthos.freeboxos.fr/tbminer-ng][tbminer-ng]  
It is possible to install it locally if need 

## Docker-Compose

On a Linux environement with docker-compose

```bash
wget https://gitlab.com/tbminer-ng/webapp/-/raw/main/docker-compose.yml

docker-compose up -d
```
Connect with the browser to [http://127.0.0.1:8004/tbminer-ng](http://127.0.0.1:8004/tbminer-ng)

you can custom docker-compose.yml :
```bash
#  ./docker-compose.yml
version: '3.8'

services:
  web:
    restart: always
    image: clecarpentier/tbminer-ng:latest
    ports:
      - 8004:8000 # change "8004" value with an available host port
    environment:
      - BASE_PATH=/tbminer-ng # the app base uri
    command: gunicorn main:app_root --workers 2 --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0:8000 --timeout 3600
```


## Docker

For docker installation, see [official docker guide](https://docs.docker.com/get-docker/)

```
docker run -p 8080:8000 -d -e BASE_PATH="/tbminer-ng" clecarpentier/tbminer-ng:latest
```
Connect with the browser to [http://127.0.0.1:8080/tbminer-ng](http://127.0.0.1:8080/tbminer-ng)

With :
 - "-p 8080:8000" for mapping container port to host. You can change 8080 port.
 - "-d" to run container background


## From Source

```
git clone https://gitlab.com/tbminer-ng/webapp.git
cd webapp
docker build --tag 'tbminer-ng' .
docker run -p 8080:8000 -d -e BASE_PATH="/tbminer-ng" tbminer-ng
```
Connect with browser to [http://127.0.0.1:8080/tbminer-ng](http://127.0.0.1:8080/tbminer-ng)

With :
 - "-p 8080:8000" for mapping container port to host. You can change 8080 port.
 - "-d" to run container background


[tbminer-ng]: https://darthos.freeboxos.fr/tbminer-ng/ "Online free platform for MTBC lineage prediction"

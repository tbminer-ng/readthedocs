# webApp CICD


Webapp TBminer-ng uses a CICD (Continuous Integration-Continuous Delivery) base on gitlab-ci to automate the integration of modification after developer actions.
In fact, after push modification on the main branch of the project on GitLab, a runner (a host link to the project) gets the project and applies the ".gitlab-ci.yaml" file.

![CICD.png](CICD.png)

```gitlab
variables:
  CI_REGISTRY: "docker.io"
  CI_IMAGE: "clecarpentier/tbminer-ng"

stages:
  - Build                            # build image and push to docker repository
  - DeployStaging                    # Deploy image on the staging server (if the build was OK)
  - DeployProd                       # Deploy image on the production server (manually)

build:
  stage: Build
  script:
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag=""
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag=":$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi

    - echo "$CI_IMAGE"
    - docker build --pull -t "$CI_IMAGE:$CI_COMMIT_SHORT_SHA" .
    - IMAGE_ID=$(docker images | grep $CI_IMAGE | grep $CI_COMMIT_SHORT_SHA | awk '{print $3}')
    - docker tag $IMAGE_ID $CI_IMAGE:latest
    - docker push "$CI_IMAGE:$CI_COMMIT_SHORT_SHA"
    - docker push "$CI_IMAGE:latest"
    - echo "$CI_COMMIT_SHORT_SHA"

  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile

deploy_staging:
  stage: DeployStaging
  script:
    - ssh $PROD_USER@192.168.1.128 "cd /home/clement/webapp && sudo docker-compose pull && sudo docker-compose up -d"

deploy_prod:
  stage: DeployProd
  when: manual
  script:
    - ssh $PROD_USER@$PROD_HOST "cd /home/clement/webapp && sudo docker-compose pull && sudo docker-compose up -d"

```

Combined with the test stage on the build  (see Dockerfile), we ensure the staging server uses the latest functional version. 

On the other hand, deploying to production requires a manual action as well as sufficient rights.

#### Build stage

On the build stage, the project was built to the docker container ```docker build [...]``` and then pushed to the docker repository with the tag *latest*.

#### Deploy stage

On the deploy stage, docker images of the docker-compose were updated, and the container was restarted.

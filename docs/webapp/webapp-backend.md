# WebApp backend

## Backend - fastAPI

```bash
.
|-- config.yaml                     # general fastapi configuration
|-- data
|   |-- mtbc_clean.csv              # data reference
|-- frontend                        # element of side angular component
|   |-- contact.yaml
|   |-- inputdescription.yaml
|   |-- otherdescription.yaml
|   `-- outputdescription.yaml
|-- main.py                         # main application 
|-- model_config                    # machine learning configuration
|   |-- clf_best_param.yaml         # parameters from jupyter subproject
|   |-- data_input_columns.yaml     # description of columns used for prediction
|   |-- data_output_columns.yaml    # description of target prediction columns
|   |-- predict_matrix.yaml         # associate input <=> output 
|   `-- resample_output_class.yaml  # oversampling parameters
|-- models                          # train models storage
|-- requirements.txt                # python package 
|-- static                          # build angular app
|-- test.py                         # test main application
`-- train_model.py                  # python script to trains models
```

Three folders are especially important : 
 * main.py
 * train_model.py
 * test.py

### main.py
The fastAPI application for REST API

Five sections are present on main.py

```
## Import
import os
[...]
from fastapi.responses import FileResponse
```
All need package was defined here

```
## Configuration file
with open("config.yaml", "r") as config_file:
    config = yaml.load(config_file, Loader=yaml.FullLoader)
[...]
with open("model_config/data_output_columns.yaml", "r") as data_output_file:
    data_output = yaml.load(data_output_file, Loader=yaml.FullLoader)
```
List configurations files and variables data used on the application.

```
## Fastapi configuration
base_path = os.getenv('BASE_PATH', config['base_path'])
FORMAT = "%(levelname)s:%(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)

app_root = FastAPI()
app = FastAPI()

app_root.mount(base_path, app)

app.mount("/static/", StaticFiles(directory="static",html = True), name="static")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
```
Define fastAPI service (logging, path, CORS, etc.)


```
## REST API funcion
@app_root.get("/", response_class=RedirectResponse)
async def redirect_fastapi():
    return config['base_path']
[...]
```
Define all functions that are accessible by the API.
It consists of a decorator ```@app_root.get("/", response_class=RedirectResponse)``` who defines the mount point for fastAPI (app_root), the method(get), relative URL ("/") and some optional elements (response_class=RedirectResponse)
And a python function ```async def redirect_fastapi():```.  
Parameters of this function were sent with the request URL.


### train_model.py
Python script for train models used for REST API predictions*

![train.png](train.png)

We reuse validate pipeline and hyperparameter from the model research


### test.py
```
client = TestClient(app)
def test_base():
    response = client.get("./")
    print(response)
    assert response.status_code == 200
[...]
```
Define unit testing launch during the docker image build after the train model.
It verifies all function responses without an error code.



# WebApp Frontend

## Frontend - Angular

```bash
.
|-- angular.json                            # Angular project parameters
|-- package.json                            # Angular package
|-- src
|   |-- app
|   |   |-- app-routing.module.ts           # Routing, for future development
|   |   |-- app.component.html              # Base app tml
|   |   |-- app.component.scss              # Base app css
|   |   |-- app.component.ts                # Base app typescript
|   |   |-- app.module.ts                   # Base app import
|   |   |-- component                       # Components of the app
|   |   |   |-- dialog-template             # Template used for side dialogue box with backend data
|   |   |   |-- header                      # Not use
|   |   |   |-- mainContent                 # Main application for check, send and get result    <=========
|   |   |   |-- nav                         # Side bar with dialog-box
|   |   |   `-- side                        # static dialogue box  TO DO: move to backend
|   |   |-- models
|   |   |   `-- dialogbox.model.ts          # Model for dialogue box data from the backend
|   |   `-- services
|   |       |-- backend.service.prod.ts     # PRODUCTION path to interact with backend
|   |       |-- backend.service.ts          # DEVELOPMENT path to interact with the backend
|   |       |-- csv-header.service.ts       # List of header names for checking CSV file
|   |       `-- tbminer-model.service.ts    # Parameters lists to interact with backend
|   |-- assets
|   |-- favicon.ico
|   |-- index.html
|   |-- main.ts
|   `-- styles.scss
|-- tsconfig.app.json                     # Angular file
|-- tsconfig.json                         # Angular file
`-- tsconfig.spec.json                    # Angular file
```

Two folders are especially important : 
 * src/app/component/mainContent
 * src/app/services

### folder src/app/component/mainContent
This is the component to upload, check, send and get predicted results with input data.

```bash
# ./component/mainContent/upload-to-predict/upload-to-predict.component.ts
```
Main application class.


The main function is described below.

```typescript
setState(control: FormControl, state: boolean) {
    [...]
}
```
Function for validating mat-step


```typescript
regexRename(): boolean {
    [...]
}
```
Rename and check column header names.


```typescript
sendToTbminerAPI(input: any): void {
    [...]
}
```
The function used to interact with the backend to make a prediction in JSON format and display them


```typescript
sendToTbminerAndDownload(input: any): void {
    [...]
}
```
The function used to interact with the backend to get a prediction on CSV format and download them



### folder src/app/services
Application variables are stored here

```bash
# ./services/backend.service.prod.ts 

[...]
  baseurl : string = '..'
[...]
```
This is where the **PRODUCTION** path to interact with the backend was defined. 
'baseurl' indicates here a relative path in production.


```bash
# ./services/backend.service.ts 

[...]
  baseurl : string = 'https://darthos.freeboxos.fr/tbminer-ng'
[...]
```
This is where **DEVELOPMENT** path to interact with the backend was defined. 
'baseurl' indicates here an absolute path for the external backend.


Other files define application variables.

### folder src/app/component/nav

This is the left panel component. Data was dynamically downloaded from the backend with HTTP GET request : 
````typescript
# ./nav.component.ts
  ngOnInit()
{
    this.http.get<DialogBoxModel[]>(this.backendService.apiInputDescription)
        .subscribe(response => {
            this.input_descriptions = response
        })
        [...]
}
````

The response was a list, then it iterate (*ngFor) and push on the html template ((click)="openDialogTemplate(input_description)")
````html
# ./nav.component.html
    <mat-nav-list>
      <a matListItemTitle></a>
      <h2 matListItemTitle>Input data</h2>
      <a mat-list-item *ngFor="let input_description of input_descriptions" matTooltip="{{input_description.title}}" (click)="openDialogTemplate(input_description)">{{ input_description.title }}</a>
      <br>
````

````{note}
To edit or add a new element on this panel, it must be made on the backend (see backend section)
````


# WebApp architecture

![RESTAPI.png](RESTAPI.png)
An example of the interaction of the user with the web application: when clicking on the *download result* button, 
the frontend (angular application) sends an HTTP POST request to the backend via the REST API (FastAPI). 
The backend computes the prediction and then sends the response to the frontend.


```bash
.
|-- .gitlab-ci.yml # CI/CD configuration must be modified to reuse
|-- Dockerfile # Use to create a docker application
|-- docker-compose.yml # Use to ease the docker deployment
|-- tbminer-angular # Frontend
|   |-- 
`-- tbminer-fastapi # Backend 
    |-- 
```


The Webapp project root contains two folders and three files.
- **.gitlab-ci.yml** contains CI/CD configurations, useful for active development. It must be modified before reuse in another environment
- **Dockerfile** define how to create the docker image
- **docker-compose.yml** can help to deploy the docker image
- **tbminer-angular** folder contain the frontend, developed with the angular framework [https://angular.io/](https://angular.io/)
- **tbminer-fastapi** folder contain the backend, developed with the FastAPI framework [https://fastapi.tiangolo.com/](https://fastapi.tiangolo.com/)

Use docker ease to deploy the application: dependency and based image layer version were defined, then it just needs 
a docker environment for serving the application.

## Docker image build
```
###
# python image used for training model
FROM python:3.9.5-slim-buster as train_model
# [...]
# train script
RUN python3 train_model.py


###
# node image used for building angular frontend app
FROM node:18-alpine AS build
# [...]
# build frontend
RUN npm run ng -- build --base-href ./


###
# Main docker app
FROM python:3.9.5-slim-buster
# [...]
# fastapi app
COPY tbminer-fastapi/ /usr/src/app

# angular build app
COPY --from=build /app/dist/tbminer-angular/ /usr/src/app/static

# xgboost models
COPY --from=train_model /usr/src/app/models/ /usr/src/app/models

# launch test
RUN pytest --cov -v test.py

# docker internal expose port
EXPOSE 8000

# fastapi launch command
CMD ["gunicorn", "main:app_root","--workers", "2","--worker-class" ,"uvicorn.workers.UvicornWorker" , "--bind", "0.0.0.0:8000", "--timeout", "3600"]
```

![build_docker.png](build_docker.png)

Before serving the application, three-step was necessary :
1. Train the model
2. Build the angular application
3. Create the final image with the train model, angular application and all necessary dependencies, 
and run the test.

We used three different images to take this for different reasons:
1. Faster the build process: docker uses cache; if one image has no modification, it uses it.
2. Use the most appropriate image
3. create a lighter production image


## Frontend - Angular

```bash
.
|-- angular.json
|-- package.json
|-- src
|   |-- app
|   |   |-- component
|   |   |   |-- mainContent
|   |   `-- services
|   |       |-- backend.service.prod.ts
|   |       |-- backend.service.ts
|   |       |-- csv-header.service.ts
|   |       `-- tbminer-model.service.ts

```

Two folders are especially important : 
 * src/app/component/mainContent
 * src/app/services

#### src/app/component/mainContent
This is the component that interacts with the backend. 

#### src/app/services
Application variables are stored here

The frontend permits the user to interact easily with the backend. 




## Backend - fastAPI

```bash
.
|-- config.yaml # general fastAPI configuration
|-- data
|   |-- mtbc_clean.csv # data reference
|-- frontend # element of side angular component
|   |-- contact.yaml
|   |-- inputdescription.yaml
|   |-- otherdescription.yaml
|   `-- outputdescription.yaml
|-- main.py # main application
|-- model_config # machine learning configuration
|   |-- clf_best_param.yaml # parameters from jupyter subproject
|   |-- data_input_columns.yaml # description of columns used for prediction
|   |-- data_output_columns.yaml # description of target prediction columns
|   |-- predict_matrix.yaml # associate input <=> output 
|   `-- resample_output_class.yaml # oversampling parameters
|-- models # train models storage
|-- requirements.txt # python package 
|-- static # build angular app
|-- test.py # test for the main application
`-- train_model.py # python script to trains models

```

Two files are especially important : 
 * main.py
 * train_model.py

#### main.py
The fastAPI application for REST API

#### train_model.py
Python script for training models during the build stage.

This is a REST API: all interaction was via HTTP request.



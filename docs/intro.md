# Introduction

This documentation explains how to deploy, use and maintain the project **tbminer-ng**

The project objective was to predict some lineage classifications of MTBC isolate based on spoligotype and/or VNTR data.

This projet contains two subproject :
 * [WebApp application](webapp/webapp)
 This is the main project application, with an online free platform. 
 [https://darthos.freeboxos.fr/tbminer-ng][tbminer-ng]

 * [Jupyter playbook](jupyter/jupyter)
Several notebooks were used to train, test and compare machine-learning modeles

[tbminer-ng]: https://darthos.freeboxos.fr/tbminer-ng/ "Online free platform for MTBC lineage prediction"
